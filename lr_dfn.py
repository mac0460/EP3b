import os
import subprocess
import numpy as np
import torch
import torch.nn as nn
import matplotlib.pyplot as plt


class LogisticRegression(nn.Module):
    """
    Logistic regression model.

    You may find nn.Linear and nn.Softmax useful here.

    :param config: hyper params configuration
    :type config: LRConfig
    """
    def __init__(self, config):
        super(LogisticRegression, self).__init__()
        # YOUR CODE HERE:
        in_features = config.height * config.width * config.channels
        out_features = config.classes
        self.add_module("linear", nn.Linear(in_features, out_features))
        self.add_module("softmax", nn.Softmax(1))
        # END YOUR CODE

    def forward(self, x):
        """
        Computes forward pass

        :param x: input tensor
        :type x: torch.FloatTensor(shape=(batch_size, number_of_features))
        :return: logits
        :rtype: torch.FloatTensor(shape=[batch_size, number_of_classes])
        """
        # YOUR CODE HERE:
        logits = self.linear(x)
        # END YOUR CODE
        return logits

    def predict(self, x):
        """
        Computes model's prediction

        :param x: input tensor
        :type x: torch.FloatTensor(shape=(batch_size, number_of_features))
        :return: model's predictions
        :rtype: torch.LongTensor(shape=[batch_size, number_of_classes])
        """
        # YOUR CODE HERE:
        logits = self.forward(x)
        y_hat = self.softmax(logits)
        predictions = torch.argmax(y_hat, dim=1)       
        # END YOUR CODE
        return predictions


class DFN(nn.Module):
    """
    Deep Feedforward Network.

    The method self._modules is useful here.
    The class nn.ReLU() is useful too.

    :param config: hyper params configuration
    :type config: DFNConfig
    """
    def __init__(self, config):
        super(DFN, self).__init__()
        # YOUR CODE HERE:
        order = []
        in_features = config.height * config.width * config.channels
        
        for out_features in (config.architecture):
            order.append(nn.Linear(in_features, out_features))
            if (out_features != config.classes):
                order.append(nn.ReLU())
                in_features = out_features
        self.add_module("g", nn.Sequential(*order))
        self.add_module("softmax", nn.Softmax(1))
        # END YOUR CODE


    def forward(self, x):
        """
        Computes forward pass

        :param x: input tensor
        :type x: torch.FloatTensor(shape=(batch_size, number_of_features))
        :return: logits
        :rtype: torch.FloatTensor(shape=[batch_size, number_of_classes])
        """
        # YOUR CODE HERE:
        logits = self.g.forward(x)
        # END YOUR CODE
        return logits

    def predict(self, x):
        """
        Computes model's prediction

        :param x: input tensor
        :type x: torch.FloatTensor(shape=(batch_size, number_of_features))
        :return: model's predictions
        :rtype: torch.LongTensor(shape=[batch_size, number_of_classes])
        """
        # YOUR CODE HERE:
        logits = self.forward(x)
        y_hat = self.softmax(logits)
        predictions = torch.argmax(y_hat, dim=1)
        # END YOUR CODE
        return predictions


def train_model_img_classification(model,
                                   config,
                                   dataholder,
                                   model_path,
                                   verbose=True):
    """
    Train a model for image classification

    :param model: image classification model
    :type model: LogisticRegression or DFN
    :param config: image classification model
    :type config: LogisticRegression or DFN
    :param dataholder: data
    :type dataholder: DataHolder
    :param model_path: path to save model params
    :type model_path: str
    :param verbose: param to control print
    :type verbose: bool
    """
    train_loader = dataholder.train_loader
    valid_loader = dataholder.valid_loader

    best_valid_loss = float("inf")
    # YOUR CODE HERE:
    # i) define the loss criteria and the optimizer. 
    # You may find nn.CrossEntropyLoss and torch.optim.SGD useful here.
    criterion = nn.CrossEntropyLoss()
    optimizer = torch.optim.SGD(model.parameters(), config.learning_rate, config.momentum)
    # END YOUR CODE

    train_loss = []
    valid_loss = []
    for epoch in range(config.epochs):
        for step, (images, labels) in enumerate(train_loader):
            # YOUR CODE HERE:
            # ii) You should zero the model gradients
            # and define the loss function for the train data.
            optimizer.zero_grad()
            images = images / 255
            logits = model(images)
            loss = criterion(logits, labels)
            # END YOUR CODE
            if step % config.save_step == 0:
                # YOUR CODE HERE:
                # iii) You should define the loss function for the valid data.
                v_images, v_labels = next(iter(valid_loader))
                v_images = v_images / 255
                v_logits = model(v_images)
                v_loss = criterion(v_logits, v_labels)
                # END YOUR CODE
                valid_loss.append(float(v_loss))
                train_loss.append(float(loss))
                if float(v_loss) < best_valid_loss:
                    msg = "\ntrain_loss = {:.3f} | valid_loss = {:.3f}".format(float(loss),float(v_loss))
                    torch.save(model.state_dict(), model_path)
                    best_valid_loss = float(v_loss)
                    if verbose:
                        print(msg, end="")
            # YOUR CODE HERE:
            # iv) You should do the back propagation
            # and do the optimization step.
            loss.backward()
            optimizer.step()  
            # END YOUR CODE
    if verbose:
        x = np.arange(1, len(train_loss) + 1, 1)
        fig, ax = plt.subplots(1, 1, figsize=(12, 5))
        ax.plot(x, train_loss, label='train loss')
        ax.plot(x, valid_loss, label='valid loss')
        ax.legend()
        plt.xlabel('step')
        plt.ylabel('loss')
        plt.title('Train and valid loss')
        plt.grid(True)
        plt.show()
